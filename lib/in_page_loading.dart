import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lori_sdk/data/lori_api.dart';
import 'package:neo_platform_widgets/platform_progress_indicator.dart';

class InPageLoading<T> extends StatefulWidget {

  final Future<ApiCallback<List<T>>> Function() apiListCall;
  final Future<ApiCallback<T>> Function() apiSingleCall;
  final void Function(List<dynamic>) apiListResult;
  final void Function(dynamic) apiSingleResult;
  final void Function(DioError) apiError;

  InPageLoading({this.apiListResult, this.apiSingleResult, this.apiListCall, this.apiSingleCall, this.apiError});

  @override
  State<StatefulWidget> createState() => _InPageLoadingState<T>();

}

class _InPageLoadingState<T> extends State<InPageLoading> {

  bool _isLoading = true;
  bool _isError = false;
  bool _isTakingTooLong = false;
  ApiCallback<dynamic> _errorResponse;

  ConnectivityResult _currentConnectivity;
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();

    _setupConnectivityListener();

    // Start the api loading
    _load();
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      return Center(
        child: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                width: 30, height: 30,
                child: PlatformProgressIndicator(large: true,),
              ),
              _isTakingTooLong ? Padding(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 25),
                child: Text('O carregamento está demorando um pouco mais do que deveria...\nPor favor aguarde mais alguns instantes :)', maxLines: 5, style: TextStyle(fontSize: 12, color: Colors.grey), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
              ) : Container()
            ],
          ),
        ),
      );
    } else if (!_isLoading && _isError) {
      return Center(
        child: Container(
          margin: EdgeInsets.all(15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(_errorResponse.resultErrorMessage == null
                  ? 'Ocorreu um erro e não foi possível completar a sua solicitação.\nVerifique sua conexão com a internet\ne tente novamente.' : _errorResponse.resultErrorMessage,
                style: TextStyle(color: Colors.black, fontSize: 13), textAlign: TextAlign.center,),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                child: SizedBox(
                  height: 35,
                  child: RaisedButton(
                    onPressed: _tryAgain,
                    child: Text('TENTAR NOVAMENTE', style: TextStyle(fontSize: 12),),
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  void dispose() {
    _removeConnectivityListener();
    // Call super
    super.dispose();
  }

  void _load() {
    // Schedule a task to the future to warn the user that the loading is taking too long
    Future.delayed(Duration(seconds: 3), () {
      if (_isLoading) {
        if (mounted) {
          setState(() => _isTakingTooLong = true);
        }
      }
    });
    // Do the loading that the widget need to
    (widget.apiListCall != null ? widget.apiListCall() : widget.apiSingleCall()).then((response) {
      // Check for success or error
      if (response.isSuccessful) {
        // Remove the loading state and return the result
        // setState(() => _isLoading = false);
        // Do the callback
        if (widget.apiListResult != null) widget.apiListResult(response.resultData);
        else if (widget.apiSingleResult != null) widget.apiSingleResult(response.resultData);
      } else {
        // If we have the callback call it
        if (widget.apiError != null && response.error != null) {
          widget.apiError(response.error);
        }
        // Flag as error and remove the loading state
        if (mounted) {
          setState(() {
            _isLoading = false;
            _isError = true;
            _errorResponse = response;
          });
        }
      }
    });
  }

  void _tryAgain() {
    setState(() {
      _isLoading = true;
      _isError = false;
      _isTakingTooLong = false;
    });
    // Try to reload on API
    _load();
  }

  void _setupConnectivityListener() async {
    //Listen to the connectivityStatus
    _connectivitySubscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      // Got a new connectivity status!
      if (result != null) {
        //On Android, the current connectivity will always be retrieved by the onChanged listener,
        // the listener will return the first result right after it is called
        //      (it shouldn't, but it will be)
        //On iOS, the listener works as expected and only notify of CHANGES
        //So, we need to add some more validations here to make sure this redundant call is handled as it should

        //We can only retry a request if some conditions are met:
        // - The request was performed and resulted in an error
        // - The request IS NOT being loaded right now
        // - The connectivity status received is different than the previous one
        // - The connectivity status is a valid one (wifi/cellular)
        if (_isError &&
            !_isLoading &&
            result != _currentConnectivity &&
            (result == ConnectivityResult.mobile ||
                result == ConnectivityResult.wifi)) {
          _tryAgain();
        }

        //We always save the result as the current connectivity
        _currentConnectivity = result;
      }
    });

    //Retrieve the _currentConnectivity manually (redundant on Android, needed on iOS)
    Connectivity().checkConnectivity().then((result) {
      if (result != null) {
        _currentConnectivity = result;
      }
    });
  }

  void _removeConnectivityListener() {
    if (_connectivitySubscription != null) {
      _connectivitySubscription.cancel();
      _connectivitySubscription = null;
    }
  }

}